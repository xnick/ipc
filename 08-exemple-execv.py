#!/usr/bin/python
#edt 2017-2018
#
#Exemple: fork / execv
#Generar subprocessos
#----------------------------------------------------------
import os,sys
print "Inici del programa principal PARE"
pid= os.fork()
if pid != 0 :
    print "Programa PARE",os.getpid(),pid
else:
    print "Programa FILL",os.getpid(),pid
    #os.execv("/usr/bin/ls",["/usr/bin/ls","-la","/"])
    #os.execl("/usr/bin/ls","/usr/bin/ls","-la","/")
    os.execlp("ls","ls","/")
print "Fi del programa",os.getpid()
sys.exit(0)


