#!/usr/bin/python
#edt ASX-M06
#exemple de popen hardcode
#Tot hardcode
#Entre com a argument un usuari
#--------------------------------------------------------------
import sys
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser(description='Consulta SQL interectiva') 
parser.add_argument("num_empl", help='Sentencia SQL a executar ',metavar='sentencoa SQL') 
args = parser.parse_args() 

cmd = "psql -h 172.17.0.2 training -U postgres "
sqlstatment="select * from repventas where num_empl=%s;" % (args.num_empl)
# POPEN
pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write(sqlstatment+"\n\q\n")
# Sortida
for line in pipeData.stdout:
	print line,
sys.exit(0)

