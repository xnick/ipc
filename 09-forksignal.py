#!/usr/bin/python
#edt 2017-2018
#-*- coding: utf-8-*-
#Exemple: fork / signal
#Un pare genera un fill i es mor.
#El fill fa bucle infinit, pero:
#       - acaba als 3 minuts
#       - amb sighub torna a comenzar els tres minuts
#       - amb sigterm mostra missatge i plega
#----------------------------------------------------------
import os,sys,signal

def mysighup(signum,frame):
    print "Reinici del temps de la alarma",signum
    signal.alarm(3*60)
    print signal.alarm(0)
def mysigterm(signum,frame):
    print "Process acabat",signum
    print signal.alarm(0)
    sys.exit(0)

signal.signal(signal.SIGTERM,mysigterm)
signal.signal(signal.SIGHUP,mysighup)
signal.alarm(3*60)

print "Inici del programa principal PARE"
pid= os.fork()
if pid != 0 :
    print "Proces PARE ha comenzat",os.getpid()
    sys.exit(0)
print "Proces fill ha comenzat",os.getpid()
while True:
    pass
sys.exit(0)
