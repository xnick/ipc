#!/usr/bin/python
#edt 2017-2018
#
#Exemple IPC
#Un pare genera un fill i es mor 
#El fill executa el programa: 06-signal.py
#----------------------------------------------------------
import signal,os,sys

pid= os.fork()
if pid != 0 :
    print "Proces PARE ha comenzat",os.getpid(),pid
    sys.exit(0)
print "Proces fill ha comenzat",os.getpid(),pid
os.execv("/usr/bin/python",["/usr/bin/python","06-eduard-ex-signal.py","3"])
