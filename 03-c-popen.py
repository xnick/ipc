#!/usr/bin/python
#edt ASX-M06
#exemple de popen hardcode
#Tot hardcode
#--------------------------------------------------------------
import sys
from subprocess import Popen, PIPE
import argparse

SQLSTATMENT="select * from oficinas;"
parser = argparse.ArgumentParser(description='Consulta SQL interectiva') 
parser.add_argument(dest='sqlStatment', help='Sentencia SQL a executar ',metavar='sentencoa SQL',default=SQLSTATMENT) 
args = parser.parse_args() 

cmd = "psql -h 172.17.0.2 training -U postgres "
# POPEN
pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write(args.sqlStatment+"\n\q\n")
# Sortida
for line in pipeData.stdout:
	print line,
sys.exit(0)

