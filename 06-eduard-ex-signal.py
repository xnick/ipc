#!/usr/bin/python
#edt 2017-2018
#
#Exemple IPC: signals
#----------------------------------------------------------
import signal,os,sys,argparse

parser = argparse.ArgumentParser(description='Consulta SQL interectiva') 
parser.add_argument(dest="n",type=int, help='Minuts que el programa estara executant') 
args = parser.parse_args() 

upper=0
down=0

def myupper(sig,frame):
    global upper
    segons=signal.alarm(0)
    signal.alarm(segons+60)
    print "Upper time signal ",sig
    upper+=1

def mydown(sig,frame):
    global down
    segons=signal.alarm(0)
    signal.alarm(segons-60)
    print "Down time signal ",sig
    down+=1

def mytime(sig,frame):
    segons=signal.alarm(0)
    signal.alarm(segons)
    print "Faltan aun uns ",segons
    print "Signal ",sig

def mytime_restart(sig,frame):
    signal.alarm(args.n*60)
    print "Time restart"
    print "Signal",sig

signal.signal(signal.SIGUSR1,myupper)
signal.signal(signal.SIGUSR2,mydown)
signal.signal(signal.SIGTERM,mytime)
signal.signal(signal.SIGHUP,mytime_restart)
signal.signal(signal.SIGINT,signal.SIG_IGN)
signal.alarm(args.n*60)

while True:
    #fem un open que es queda encallat
    pass

signal.alarm(0)
sys.exit(0)
