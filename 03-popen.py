#!/usr/bin/python
#edt ASX-M06
#exemple de popen hardcode
#Tot hardcode
#--------------------------------------------------------------
import sys
from subprocess import Popen, PIPE
commandLocal  = "psql -qtA -F ';' laboratori_clinic -U postgres -w jupiter -c 'select * from pacients;'" 
#commandRemote  = "psql -qtA -h i17 -F ';' clinic -U postgres -w jupiter -c 'select * from pacients;'"
commandRemote = "psql -h 172.17.0.2 training -U edtasixm06 -c 'select * from oficinas;'"
# POPEN
pipeData = Popen(commandRemote, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
# Sortida
for line in pipeData.stdout.readlines():
	print line,
sys.exit(0)

